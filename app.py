from flask import Flask, request, jsonify
import jwt
from jwt import InvalidTokenError

app = Flask(__name__)


@app.route("/")
def login():
    return f"<a href='https://saml-preprod.digilab.network/login?state=digid-client'>Login met DigiD</a>"


@app.route("/logout")
def logout():
    return f"<a href='https://saml-preprod.digilab.network/logout'>Logout</a>"


@app.route("/whoami")
def whoami():
    # Get the token from the query parameters
    jwt_token = request.args.get('token')

    if not jwt_token:
        return jsonify({"error": "JWT token not found in query parameters"}), 400

    with open("digid-proxy-public.pem", "r") as f:
        public_key = f.read()

    try:
        decoded_jwt = jwt.decode(jwt_token, public_key, algorithms=["RS256"], options={"verify_aud": False, "verify_signature": True})

        subject = decoded_jwt.get("sub")
        return jsonify({"subject": subject})

    except InvalidTokenError as e:
        return jsonify({"error": f"Invalid token: {e}"}), 400


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=8080, debug=True)
