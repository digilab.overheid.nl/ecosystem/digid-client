FROM python:3.12

ENV FLASK_APP=app.py \
    FLASK_HOST="0.0.0.0" \
    FLASK_PORT="8080"

RUN pip install flask pyjwt cryptography

WORKDIR /app
COPY app.py digid-proxy-public.pem ./

CMD ["python", "app.py"]
